# Counter API with Python and Flask
This repo contains files needed to create an API using Python and Flask, in order to count webpage hits. The application is bundled into a container and deployed to a kubernetes cluster. The backend database is Redis.

# API functionality

Description: Retrives number of hits for the current deployment.

Request:       `GET /`

Response:     `HTTP/1.1 200 OK`
`Wonderful.. We have hit X times`

### Details

The app is written in Python, using Flask framework 

 - `app.py` is the actual app code
 - `requirements.txt` are the dependencies required to run the app
 - `Dockerfile` is used to build docker container
 
 ### Building/testing steps

Download/pull this repository:
`git clone https://gitlab.com/rrksrb/hitcounter.git`

Go to the newly created directory
`cd hitcounter`

Build and tag your docker image

    $ docker build -t hit-counter . 
    Sending build context to Docker daemon  7.68 kB
    Step 1/6 : FROM python:2-alpine
    ---> 5082b69714da
    Step 2/6 : WORKDIR /usr/src/app
    ---> bce5e8d2f953
    Removing intermediate container af42586f92d6
    Step 3/6 : COPY requirements.txt ./
    ---> c932521438b6
    Removing intermediate container d57bc7bcc4d0
    Step 4/6 : RUN pip install --no-cache-dir -r requirements.txt
    ...
    Step 5/6 : COPY . .
    ---> d48e58cb701e
    Removing intermediate container cd12af31d77d
    Step 6/6 : CMD python ./app.py
    ---> Running in 9bab103fafb7
    ---> 95982cf9d7c8
    Removing intermediate container 9bab103fafb7
    Successfully built 95982cf9d7c8

Make sure to push the image to docker hub:

    $ docker tag hit-counter rrksrb/hit-counter:1.0.0
    
    $ docker push rrkrb/hit-counter:1.0.0
    The push refers to a repository [docker.io/calinrus/hit-counter]
    898d7f171bf4: Pushed 
    48f330753248: Pushed 
    8096265db166: Pushed 
    60af20e2cf1f: Pushed 
    9d1f139ac886: Layer already exists 
    029d8a704a27: Layer already exists 
    00023a62e045: Layer already exists 
    73046094a9b8: Layer already exists 
    latest: digest: sha256:b2cc2202c58c182daa399016716b679d5a0b36e402ad21149ebb09472f908383 size: 1993

    

 ### Deployment steps
I have tested this on Minikube Engine. Let's check that we have the cluster available:


#### 2. Using a _Deployment_ controller
To create a Deployment, run the following command:
    
    $ cd k8s
    
    $ kubectl create -f deployment.yml 
    deployment.apps/myapp-deployment created
    deployment.apps/redis-deployment created

    $ kubectl get deployments
    NAME               DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    myapp-deployment   3         3         3            3           59s
    redis-deployment   1         1         1            1           59s
    
We still need to create a _Service_ in order for our app to communicate with Redis and outside world.

    $ kubectl create -f services.yml 
    service/myapp-lb created
    service/redis-lb created
    
$ kubectl get services
NAME             TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
kubernetes       ClusterIP   10.96.0.1        <none>        443/TCP        2d23h
myapp-lb         NodePort    10.106.96.93     <none>        80:30409/TCP   2d19h
redis-exporter   ClusterIP   10.107.194.67    <none>        9121/TCP       2d18h
redis-lb         ClusterIP   10.110.141.197   <none>        6379/TCP       2d19h
    
Test the Deployment:

PS C:\Users\ADMIN\Desktop\hit-counter> kubectl port-forward svc/myapp-lb 80:80     
Forwarding from 127.0.0.1:80 -> 5000
Forwarding from [::1]:80 -> 5000
Handling connection for 80
Handling connection for 80

Open browser and hit 127.0.0.1 or open Other cli hitl curl command
$ curl 127.0.0.1
Wonderful.. We have hit 2 times 


### prometheus and grafana setup
kubectl create ns monitoring
kubectl apply -f kubernetes-prometheus
it will install all promethues, node-exporter and grafana.

kubectl port-forward svc/grafana 3000:3000 -n monitoring
login to  grafana and import dashbords 
for redis 11835
for memory usage 405
we can create alerts in grafana also

if you intrested setting up alerts using alerts manager install it using

kubectl apply -f kubernetes-alert-manager


#### Cleanup
To do some cleanup, run the following commands:

    $ kubectl delete deployment myapp-deployment
    deployment.extensions "myapp-deployment" deleted
    $ kubectl delete deployment redis-depoyment
    deployment.extensions "redis-deployment" deleted
    
    $ kubectl get deployments
    No resources found.
    
    $ kubectl delete service myapp-lb
    service "myapp-lb" deleted
    $ kubectl delete service redis-lb
    service "redis-lb" deleted
    
    $ kubectl get services
    NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
    kubernetes   ClusterIP   10.39.240.1   <none>        443/TCP 
